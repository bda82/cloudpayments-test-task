import os
from bottle import route, run, request
import requests
import json

client_id = os.environ.get("YA_CLIENT_ID", "8f0631aac1b24e2babbd1c2906e8e356")
client_secret = os.environ.get("YA_SECRET", "088f3bb678cd4e17a45ac18bdd79885c")


@route("/", method="GET")
def index():
    return """
    <h3>Use the next link to get YA Code</h3>
    <a target="_blank" href="https://oauth.yandex.ru/authorize?response_type=code&
    client_id=8f0631aac1b24e2babbd1c2906e8e356">Get YA Code ---></a>
    <h3>Send YA Code</h3>
    <form action="/code" method="POST">
        <input name="code" type="text" />
        <input name="submit" type="submit" value="Send Code" />
    </form>
    """


@route("/code", method="POST")
def send_code():
    query = {
        "grant_type": "authorization_code",
        "code": request.forms.get("code"),
        "client_id": client_id,
        "client_secret": client_secret,
    }

    header = {"Content-Type": "application/x-www-form-urlencoded"}

    result = requests.post("https://oauth.yandex.ru/token", query, header)

    if result.status_code == 200:
        token = json.loads(result.text)["access_token"]
        return f"""
        <h3>Your access token is: {token}</h3>
        """
    else:
        return """
        <h3>Some errors in request appears!</h3>
        """


# Запускаем веб-сервер
run(host="localhost", port=8001, quiet=False)
