from dataclasses import dataclass
from typing import Optional, Any, Dict

from aiohttp import TCPConnector, BasicAuth

from errors.card_charge_error import CardChargeError
from errors.client_secrets_error import ClientSecretsError

from abstract_client.abstract_interaction_client import AbstractInteractionClient
from errors.complete_3d_secure_error import Complete3DSecureError
from errors.payment_confirm_error import PaymentConfirmError
from errors.refund_error import RefundError
from errors.token_charge_error import TokenChargeError
from errors.yandex_payment_error import YandexPaymentError
from schemas.complete_3d_secure_request_schema import complete_3d_secure_schema
from schemas.model_schema import ModelSchema
from schemas.payment_card_charge_request_schema import payment_card_charge_schema
from schemas.payment_confirm_request_schema import payment_confirm_request_schema
from schemas.payment_token_request_schema import payment_topken_request_schema
from schemas.refund_request_schema import refund_request_schema
from schemas.refund_response_schema import refund_response_schema
from schemas.test_response_schema import test_response_schema
from schemas.void_payment_request_schema import void_payment_request_schema
from schemas.void_payment_response_schema import void_payment_response_schema
from schemas.ya_crypto_request_schema import ya_crypto_request_schema
from schemas.common_response_schema import common_response_schema


class CloudPaymentsClient(AbstractInteractionClient):
    @dataclass
    class APIUrls:
        test: str = "test"
        payment_yandex_charge: str = "payments/charge"
        payment_cards_charge: str = "payments/cards/charge"
        payment_cards_auth: str = "payments/cards/auth"
        payment_token_charge: str = "payments/tokens/charge"
        payment_token_auth: str = "payments/tokens/auth"
        payment_cards_post3d: str = "payments/cards/post3ds"
        payment_confirm: str = "payment/confirm"
        payment_refund: str = "/payments/refund"
        payment_unblock: str = "/payments/void"

    _public_id: Optional[str] = None
    _api_secret: Optional[str] = None
    _service_name: str = "CloudPayments"
    _base_url: str = "https://api.cloudpayments.ru/"
    _api_urls: APIUrls = APIUrls()

    def __init__(self, public_id: Optional[str], api_secret: Optional[str]):
        super(CloudPaymentsClient, self).__init__()
        self.CONNECTOR = TCPConnector()
        self.SERVICE = self._service_name

        if not public_id or not api_secret:
            raise ClientSecretsError(service=self._service_name)

        self._public_id = public_id
        self._api_secret = api_secret

    def _auth(self) -> BasicAuth:
        """
        Basic Auth from secret keys
        :return: BasicAuth
        """
        return BasicAuth(self._public_id, self._api_secret)

    def _make_url(self, uri: str) -> str:
        """
        Make url from base url
        :param uri: CP API URI
        :return: Full URL
        """
        return f"{self._base_url}{uri}"

    @property
    async def connected(self):
        """
        Simple test to check CP API connection with "test" URI
        :return:
        """
        response_data = await self.make_get(self._api_urls.test)
        valid_data = test_response_schema().load(data=response_data)
        return valid_data.Success

    async def make_get(self, uri: str, **kwargs: Any) -> Dict[str, Any]:
        """
        GET Request overrided with Basic Auth
        :param uri: CP API URI
        :param kwargs: arguments
        :return: CP API Response
        """
        return await self.get(
            interaction_method="get",
            url=self._make_url(uri),
            auth=self._auth(),
            **kwargs,
        )

    async def make_post(self, uri: str, **kwargs: Any) -> Dict[str, Any]:
        """
        POST Request overrided with Basic Auth
        :param uri: CP API URI
        :param kwargs: arguments
        :return: CP API Response
        """
        return await self.post(
            interaction_method="post",
            url=self._make_url(uri),
            auth=self._auth(),
            **kwargs,
        )

    async def payment_yandex_charge(
        self,
        amount: float,
        cryptogram: Any,
        currency: str = "RUB",
        description: str = "Test YA payment",
    ) -> ModelSchema:
        """
        Yandex Pay via API CloudPayments
        :param amount: Payment amount
        :param cryptogram: Cryptogram
        :param currency: Currency : RUB/USD/EUR/GBP
        :param description: Payment description
        :return: ModelSchema of response
        """
        params = {
            "PublicId": self._public_id,
            "Amount": amount,
            "Currency": currency,
            "Description": description,
            "CardCryptogramPacket": cryptogram,
        }

        ya_crypto_request_schema().load(data=params)

        response_data = await self.make_post(
            self._make_url(self._api_urls.payment_yandex_charge), json=params
        )

        validated_response = common_response_schema().load(data=response_data)

        if validated_response.Success:
            return validated_response.Model

        raise YandexPaymentError(
            reason_code=validated_response.Model.ReasonCode,
            service=self.SERVICE,
            message=validated_response.Message,
        )

    async def payment_card_charge(
        self,
        amount,
        ip_address,
        cryptogram,
        name: str,
        require_confirmation: bool = False,
        currency: str = "RUB",
        description: str | None = None,
        account_id: str | None = None,
        email: str | None = None,
        json_data: dict | None = None,
    ) -> ModelSchema:
        """
        CP API Payment with Card Charge/Auth
        :param amount: Payment amount
        :param ip_address: Payer's IP address
        :param cryptogram: Cryptogram
        :param name: Cardholder name in latin
        :param require_confirmation: If True - use two-stage payment scheme, else - one-stage payment scheme
        :param currency: Currency: RUB/USD/EUR/GBP
        :param description: Payment description
        :param account_id: Payer's ID (endpoint customer)
        :param email: E-mail of that user
        :param json_data: Any other data that will be associated with transaction including instructions
        :return: ModelSchema of response
        """
        params = {
            "Amount": amount,
            "Currency": currency,
            "IpAddress": ip_address,
            "CardCryptogramPacket": cryptogram,
            "Name": name,
        }
        params.update({"Description": description}) if description else False
        params.update({"AccountId": account_id}) if account_id else False
        params.update({"Email": email}) if email else False
        params.update({"JsonData": json_data}) if json_data else False

        payment_card_charge_schema().load(data=params)

        url = (
            self._api_urls.payment_cards_auth
            if require_confirmation
            else self._api_urls.payment_cards_charge
        )

        response_data = await self.make_post(self._make_url(url), json=params)

        validated_response = common_response_schema().load(data=response_data)

        if validated_response.Success:
            return validated_response.Model

        raise CardChargeError(
            reason_code=validated_response.Model.ReasonCode,
            service=self.SERVICE,
            message=validated_response.Message,
        )

    async def payment_token_charge(
        self,
        amount: float,
        account_id: str,
        token: str,
        currency: str = "RUB",
        require_confirmation=False,
        ip_address: str | None = None,
        invoice_id: str | None = None,
        description: str | None = None,
        email: str | None = None,
        json_data: dict | None = None,
    ) -> ModelSchema:
        """
        CP API Payment with Token Charge/Auth
        :param amount: Payment amount
        :param account_id: Payer's ID (endpoint customer)
        :param token: Card tokens issued by the system. You get it with the first successful payment
        :param currency: Currency: RUB/USD/EUR/GBP
        :param require_confirmation: If True - use two-stage payment scheme, else - one-stage payment scheme
        :param ip_address: Payer's IP address
        :param invoice_id: Order or invoice number
        :param description: Payment description
        :param email: E-mail of that user
        :param json_data: Any other data that will be associated with transaction including instructions
        :return: ModelSchema of response
        """
        params = {
            "Amount": amount,
            "Currency": currency,
            "AccountId": account_id,
            "Token": token,
        }
        params.update({"IpAddress": ip_address}) if ip_address else False
        params.update({"InvoiceId": invoice_id}) if invoice_id else False
        params.update({"Description": description}) if description else False
        params.update({"Email": email}) if email else False
        params.update({"JsonData": json_data}) if json_data else False

        payment_topken_request_schema().load(data=params)

        url = (
            self._api_urls.payment_token_auth
            if require_confirmation
            else self._api_urls.payment_token_charge
        )

        response_data = await self.make_post(self._make_url(url), json=params)

        validated_response = common_response_schema().load(data=response_data)

        if validated_response.Success:
            return validated_response.Model

        raise TokenChargeError(
            reason_code=validated_response.Model.ReasonCode,
            service=self.SERVICE,
            message=validated_response.Message,
        )

    async def complete_3d_secure(self, transaction_id: int, pa_res: str) -> ModelSchema:
        """
        Complete 3D secure payment
        :param transaction_id: MD parameter value (Systems transaction number)
        :param pa_res: PaRes value
        :return: ModelSchema of response
        """
        params = {"TransactionId": transaction_id, "PaRes": pa_res}

        complete_3d_secure_schema().load(data=params)

        response_data = await self.make_post(
            self._api_urls.payment_cards_post3d, json=params
        )

        validated_response = common_response_schema().load(data=response_data)

        if validated_response.Success:
            return validated_response.Model

        raise Complete3DSecureError(
            service=self.SERVICE,
            transaction_id=transaction_id,
            message=validated_response.Message,
        )

    async def payment_confirm(
        self, transaction_id: int, amount: float, json_data: dict | None = None
    ):
        """
        Payment Confirm
        :param transaction_id: Systems transaction number
        :param amount: Payment amount
        :param json_data:
        :return: Any other data that will be associated with transaction including instructions
        """
        params = {"Amount": amount, "TransactionId": transaction_id}
        params.update({"JsonData": json_data}) if json_data else False

        payment_confirm_request_schema().load(data=params)

        response_data = await self.make_post(
            self._api_urls.payment_confirm, json=params
        )

        validated_response = common_response_schema().load(data=response_data)

        if validated_response.Success:
            return validated_response.Model

        raise PaymentConfirmError(
            service=self.SERVICE,
            transaction_id=transaction_id,
            message=validated_response.Message,
        )

    async def void_payment(self, transaction_id: int) -> bool:
        """
        Payment Cancellation
        :param transaction_id: Systems transaction number
        :return: True/False
        """
        params = {"TransactionId": transaction_id}

        void_payment_request_schema().load(params)

        response_data = await self.make_post(
            self._api_urls.payment_unblock, json=params
        )

        validated_response = void_payment_response_schema().load(data=response_data)

        if validated_response.Success:
            return True

        return False

    async def refund(self, transaction_id: int, amount: float) -> int:
        """
        Refund
        :param transaction_id: Systems transaction number
        :param amount: Payment amount
        :return: transaction_id
        """
        params = {"Amount": amount, "TransactionId": transaction_id}

        refund_request_schema().load(params)

        response_data = await self.make_post(self._api_urls.payment_refund, json=params)

        validated_response = refund_response_schema().load(data=response_data)

        if validated_response.Success:
            return validated_response.Model.TransactionId

        raise RefundError(
            service=self.SERVICE,
            transaction_id=transaction_id,
            message=validated_response.Message,
        )


def get_cloudpayments_client(public_id, api_secret):
    """
    CloudPaymentsClient getter for DI
    :param public_id: secret Public ID
    :param api_secret: secret API key
    :return:
    """
    return CloudPaymentsClient(public_id=public_id, api_secret=api_secret)
