from dataclasses import dataclass, field

import marshmallow_dataclass as mdc
import marshmallow.validate as mv


@dataclass
class Complete3DSecureRequestSchema:
    TransactionId: int = field(metadata={"required": True, "validate": mv.Range(min=0)})
    PaRes: str = field(metadata={"required": True, "validate": mv.Length(min=1)})


complete_3d_secure_schema = mdc.class_schema(Complete3DSecureRequestSchema)
