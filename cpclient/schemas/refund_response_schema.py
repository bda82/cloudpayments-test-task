from dataclasses import dataclass, field
from typing import Optional

import marshmallow_dataclass as mdc

from schemas.model_schema import ModelSchema


@dataclass
class RefundResponseSchema:
    Success: bool = field(metadata={"required": True})
    Message: Optional[str]
    Model: Optional[ModelSchema]


refund_response_schema = mdc.class_schema(RefundResponseSchema)
