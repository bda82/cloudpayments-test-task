from dataclasses import dataclass, field
from typing import Optional

import marshmallow_dataclass as mdc

from schemas.model_schema import ModelSchema


@dataclass
class VoidPaymentResponseSchema:
    Success: bool = field(metadata={"required": True})
    Message: Optional[str]
    Model: Optional[ModelSchema]


void_payment_response_schema = mdc.class_schema(VoidPaymentResponseSchema)
