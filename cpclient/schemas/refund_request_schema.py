from dataclasses import dataclass, field

import marshmallow_dataclass as mdc
import marshmallow.validate as mv


@dataclass
class RefundRequestSchema:
    Amount: float = field(metadata={"required": True, "validate": mv.Range(min=0.1)})
    TransactionId: int = field(metadata={"required": True, "validate": mv.Range(min=0)})


refund_request_schema = mdc.class_schema(RefundRequestSchema)
