from dataclasses import dataclass, field
from typing import Optional

import marshmallow_dataclass as mdc


@dataclass
class PayerSchema:
    FirstName: str
    LastName: str
    MiddleName: str
    Birth: str
    Street: str
    Address: str
    City: str
    Country: str
    Phone: str
    Postcode: str


@dataclass
class PaymentCardChargeSchema:
    Amount: float = field(metadata={"required": True})
    IpAddress: str = field(metadata={"required": True})
    CardCryptogramPacket: str | bytes = field(metadata={"required": True})
    Currency: str = field(metadata={"required": False})
    Name: str = field(metadata={"required": False})
    PaymentUrl: Optional[str] = field(metadata={"required": False})
    InvoiceId: str = field(metadata={"required": False})
    Description: str = field(metadata={"required": False})
    CultureName: str = field(metadata={"required": False})
    AccountId: str = field(metadata={"required": False})
    Email: str = field(metadata={"required": False})
    Payer: Optional[PayerSchema] = field(metadata={"required": False})
    JsonData: Optional[dict] = field(metadata={"required": False})


payment_card_charge_schema = mdc.class_schema(PaymentCardChargeSchema)
