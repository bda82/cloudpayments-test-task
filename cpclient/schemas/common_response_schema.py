from dataclasses import dataclass, field

import marshmallow_dataclass as mdc
import marshmallow.validate as mv

from schemas.model_schema import ModelSchema


@dataclass
class CommonResponseSchema:
    Success: bool = field(metadata={"required": True})
    Message: str = field(metadata={"validate": mv.Length(min=1)})
    Model: ModelSchema


common_response_schema = mdc.class_schema(CommonResponseSchema)
