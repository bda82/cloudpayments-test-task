from dataclasses import dataclass, field
from typing import Optional

import marshmallow_dataclass as mdc
import marshmallow.validate as mv


@dataclass
class YACryptoRequestSchema:
    PublicId: str = field(metadata={"required": True, "validate": mv.Length(min=1)})
    Amount: float = field(metadata={"required": True, "validate": mv.Range(min=0.1)})
    Currency: float = field(
        metadata={"required": True, "validate": mv.OneOf(["RUB", "USD"])}
    )
    Description: Optional[str]
    CardCryptogramPacket: bytes = field(metadata={"required": True})


ya_crypto_request_schema = mdc.class_schema(YACryptoRequestSchema)
