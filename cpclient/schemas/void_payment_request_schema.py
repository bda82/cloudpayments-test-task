from dataclasses import dataclass, field

import marshmallow_dataclass as mdc
import marshmallow.validate as mv


@dataclass
class VoidPaymentRequestSchema:
    TransactionId: int = field(metadata={"required": True, "validate": mv.Range(min=0)})


void_payment_request_schema = mdc.class_schema(VoidPaymentRequestSchema)
