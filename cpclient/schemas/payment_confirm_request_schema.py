from dataclasses import dataclass, field
from typing import Optional

import marshmallow_dataclass as mdc
import marshmallow.validate as mv


@dataclass
class PaymentConfirmRequestSchema:
    Amount: float = field(metadata={"required": True, "validate": mv.Range(min=0.1)})
    TransactionId: int = field(metadata={"required": True, "validate": mv.Range(min=0)})
    JsonData: Optional[dict] = field(metadata={"required": False})


payment_confirm_request_schema = mdc.class_schema(PaymentConfirmRequestSchema)
