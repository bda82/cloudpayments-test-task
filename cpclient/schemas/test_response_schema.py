from dataclasses import dataclass, field

import marshmallow_dataclass as mdc
import marshmallow.validate as mv


@dataclass
class TestResponseSchema:
    Success: bool
    Message: str = field(metadata={"validate": mv.Length(min=1)})


test_response_schema = mdc.class_schema(TestResponseSchema)
