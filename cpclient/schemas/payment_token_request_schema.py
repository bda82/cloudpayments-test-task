from dataclasses import dataclass, field
from typing import Optional

import marshmallow_dataclass as mdc


@dataclass
class PaymentTokenRequestSchema:
    Amount: float = field(metadata={"required": True})
    AccountId: str = field(metadata={"required": True})
    Token: str = field(metadata={"required": True})
    Currency: str = field(metadata={"required": False})
    InvoiceId: str = field(metadata={"required": False})
    Description: str = field(metadata={"required": False})
    IpAddress: str = field(metadata={"required": False})
    Email: str = field(metadata={"required": False})
    JsonData: Optional[dict] = field(metadata={"required": False})


payment_topken_request_schema = mdc.class_schema(PaymentTokenRequestSchema)
