import os
import asyncio

from cpclient.cpclient import get_cloudpayments_client

public_id = os.environ.get("CP_PUBLIC_ID", "pk_44ab0e3a4eedb9b6cd0a04d9fbe32")
api_secret = os.environ.get("CP_API_SECRET", "0bf6059966198fe620047f1148699522")

uri = "test"


async def test__run_cp_test_api_call():
    """
    Integration test for CloudPaymentsClient.connected method/property
    :return: None
    """
    client = get_cloudpayments_client(public_id=public_id, api_secret=api_secret)

    c = await client.connected

    assert c


if __name__ == "__main__":
    asyncio.run(test__run_cp_test_api_call())
