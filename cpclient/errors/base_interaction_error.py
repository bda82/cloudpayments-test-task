class BaseInteractionError(Exception):
    default_message = "Backend interaction error"

    def __init__(self, *, service, method, message=None):
        self.message = message or self.default_message
        self.service = service
        self.method = method

    @property
    def name(self):
        return self.__class__.__name__

    def __str__(self):
        return (
            f"{self.__class__.__name__}({self.service}, {self.method}): {self.message}"
        )
