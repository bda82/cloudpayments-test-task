from typing import Any, Optional

from errors.base_interaction_error import BaseInteractionError


class ClientSecretsError(BaseInteractionError):
    """
    Custom Client Secrets Error BaseInteractionError
    """

    default_message = "Client Secrets are empty"

    def __init__(self, *, service: Any, method: Optional[str] = None):
        super(ClientSecretsError, self).__init__(service=service, method=method)
