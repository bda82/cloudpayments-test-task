from typing import Optional

from errors.interaction_response_error import InteractionResponseError


class Complete3DSecureError(InteractionResponseError):
    """
    Custom Complete 3D Secure operation Error InteractionResponseError
    """

    def __init__(
        self,
        *,
        service: Optional[str] = None,
        transaction_id: Optional[int] = None,
        message: Optional[str] = None,
    ):
        super(Complete3DSecureError, self).__init__(
            message=f"Cant complete 3D Secure for transaction: {transaction_id}. Message: {message}",
            status_code=500,
            method="POST",
            service=service,
            params=None,
        )
