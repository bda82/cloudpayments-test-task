from typing import Optional

from errors.interaction_response_error import InteractionResponseError


class TokenChargeError(InteractionResponseError):
    """
    Custom Token Charge operation Error InteractionResponseError
    """

    def __init__(
        self,
        *,
        service: Optional[str] = None,
        reason_code: Optional[int] = None,
        message: Optional[str] = None,
    ):
        super(TokenChargeError, self).__init__(
            message=f"Cant complete Token Charge payment. Reason Code: {reason_code}. Message: {message}",
            status_code=500,
            method="POST",
            service=service,
            params=None,
        )
