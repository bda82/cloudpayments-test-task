from typing import Optional

from errors.interaction_response_error import InteractionResponseError


class YandexPaymentError(InteractionResponseError):
    """
    Custom Yandex Crypto Token Payment operation Error InteractionResponseError
    """

    def __init__(
        self,
        *,
        service: Optional[str] = None,
        reason_code: Optional[int] = None,
        message: Optional[str] = None,
    ):
        super(YandexPaymentError, self).__init__(
            message=f"Cant complete YA payment. Reason Code: {reason_code}. Message: {message}",
            status_code=500,
            method="POST",
            service=service,
            params=None,
        )
