from typing import Optional

from errors.interaction_response_error import InteractionResponseError


class RefundError(InteractionResponseError):
    """
    Custom Refund operation Error InteractionResponseError
    """

    def __init__(
        self,
        *,
        service: Optional[str] = None,
        transaction_id: Optional[int] = None,
        message: Optional[str] = None,
    ):
        super(RefundError, self).__init__(
            message=f"Cant complete Refund for transaction: {transaction_id}. Message: {message}",
            status_code=500,
            method="POST",
            service=service,
            params=None,
        )
