from typing import Optional

from errors.interaction_response_error import InteractionResponseError


class PaymentConfirmError(InteractionResponseError):
    """
    Custom Confirm operation Error InteractionResponseError
    """

    def __init__(
        self,
        *,
        service: Optional[str] = None,
        transaction_id: Optional[int] = None,
        message: Optional[str] = None,
    ):
        super(PaymentConfirmError, self).__init__(
            message=f"Cant complete Payment Confirm for transaction: {transaction_id}. Message: {message}",
            status_code=500,
            method="POST",
            service=service,
            params=None,
        )
