.PHONY: fmt, lint

fmt:
	black .

lint:
	flake8 . --exclude venv --ignore E501