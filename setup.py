import os
from distutils.core import setup


def __read__(file_name):
    return open(os.path.join(os.path.dirname(__file__), file_name)).read()


setup(
    name="CloudPayments Test Task",
    version="0.0.12",
    description="CloudPayments Test Task",
    author_email="dmitry.bespalov@websailors.pro",
    author="dmitry.bespalov",
    url="https://websailors.pro",
    packages=[
        "cpclient",
    ],
    long_description=__read__("README.md"),
    classifiers=[
        "Development Status :: 5 - Alpha",
        "Topic :: Utilities",
        "Environment :: X11 Applications",
        "Intended Audience :: Python Developers" "License :: GNU",
    ],
    setup_requires=[
        "aiohttp==3.8.1",
        "aiosignal==1.2.0",
        "async-timeout==4.0.2",
        "attrs==22.1.0",
        "charset-normalizer==2.1.0",
        "frozenlist==1.3.1",
        "idna==3.3",
        "marshmallow==3.17.0",
        "marshmallow-dataclass==8.5.8",
        "multidict==6.0.2",
        "mypy-extensions==0.4.3",
        "packaging==21.3",
        "pyparsing==3.0.9",
        "typing-inspect==0.7.1",
        "typing_extensions==4.3.0",
        "yarl==1.8.1",
    ],
    install_requires=[
        "aiohttp==3.8.1",
        "aiosignal==1.2.0",
        "async-timeout==4.0.2",
        "attrs==22.1.0",
        "charset-normalizer==2.1.0",
        "frozenlist==1.3.1",
        "idna==3.3",
        "marshmallow==3.17.0",
        "marshmallow-dataclass==8.5.8",
        "multidict==6.0.2",
        "mypy-extensions==0.4.3",
        "packaging==21.3",
        "pyparsing==3.0.9",
        "typing-inspect==0.7.1",
        "typing_extensions==4.3.0",
        "yarl==1.8.1",
    ],
    scripts=["cpclient/cpclient.py"],
)
